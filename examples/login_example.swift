
import Foundation
import AutomataToolkit

final class LoginAction: AutomataAction {

    func actionPerformed(data: [String : AnyObject]) -> (AutomataInput, AutomataDataDictionary) {
        print("LoginAction executed")
        return (LoginInputs.LoginSucceeded.rawValue, data)
    }
}

final class GetUserProfileAction: AutomataAction {

    func actionPerformed(data: [String : AnyObject]) -> (AutomataInput, AutomataDataDictionary) {
        print("GetUserProfileAction executed")
        return (LoginInputs.GetUserProfileSucceeded.rawValue, data)
    }
}

final class GetRecentSongsAction: AutomataAction {

    func actionPerformed(data: [String : AnyObject]) -> (AutomataInput, AutomataDataDictionary) {
        print("GetRecentSongsAction executed")
        return (LoginInputs.GetRecentSongsSucceeded.rawValue, data)
    }
}

final class SuccessAction: AutomataAction {

    func actionPerformed(data: [String : AnyObject]) -> (AutomataInput, AutomataDataDictionary) {
        print("SuccessAction executed")
        return (LoginInputs.LoginSucceeded.rawValue, data)
    }
}

final class FailureAction: AutomataAction {

    func actionPerformed(data: [String : AnyObject]) -> (AutomataInput, AutomataDataDictionary) {
        print("FailureAction executed")
        return (LoginInputs.LoginSucceeded.rawValue, data)
    }
}

enum LoginStates: AutomataState {
    case Login
    case GetUserProfile
    case GetRecentSongs
    case Success
    case Failure
}

enum LoginInputs: AutomataInput {
    case LoginSucceeded
    case LoginFailed
    case GetUserProfileSucceeded
    case GetUserProfileFailed
    case GetRecentSongsSucceeded
    case GetRecentSongsFailed
}

func test123() {

    let LoginSuccessLink = NextStateLink(currentState: LoginStates.Login.rawValue,
                                         stateInput: LoginInputs.LoginSucceeded.rawValue)

    let LoginFailLink = NextStateLink(currentState: LoginStates.Login.rawValue,
                                      stateInput: LoginInputs.LoginFailed.rawValue)

    let GetUserProfileSuccessLink = NextStateLink(currentState: LoginStates.GetUserProfile.rawValue,
                                                  stateInput: LoginInputs.GetUserProfileSucceeded.rawValue)

    let GetUserProfileFailLink = NextStateLink(currentState: LoginStates.GetUserProfile.rawValue,
                                               stateInput: LoginInputs.GetUserProfileFailed.rawValue)


    let GetRecentSongsSuccessLink = NextStateLink(currentState: LoginStates.GetRecentSongs.rawValue,
                                                  stateInput: LoginInputs.GetRecentSongsSucceeded.rawValue)

    let GetRecentSongsFailLink = NextStateLink(currentState: LoginStates.GetRecentSongs.rawValue,
                                               stateInput: LoginInputs.GetRecentSongsFailed.rawValue)


    let configurationMap = [LoginSuccessLink: LoginStates.GetUserProfile.rawValue,
                            LoginFailLink: LoginStates.Failure.rawValue,
                            GetUserProfileSuccessLink: LoginStates.GetRecentSongs.rawValue,
                            GetUserProfileFailLink: LoginStates.Failure.rawValue,
                            GetRecentSongsSuccessLink: LoginStates.Success.rawValue,
                            GetRecentSongsFailLink: LoginStates.Failure.rawValue,
                            ]

    let stateMap: [AutomataState : AutomataAction.Type] = [LoginStates.Login.rawValue: LoginAction.self,
                                                           LoginStates.GetRecentSongs.rawValue: GetRecentSongsAction.self,
                                                           LoginStates.GetUserProfile.rawValue: GetUserProfileAction.self,
                                                           LoginStates.Success.rawValue: SuccessAction.self,
                                                           LoginStates.Failure.rawValue: FailureAction.self]

    let automata = Automata(
        configurationMap: configurationMap,
        initialState: LoginStates.Login.rawValue,
        stateMap: stateMap
    )

    let x = AutomataExcutor(automata: automata)

    x.run(data: [:])

    x.waitTillExecutionIsCompleted()

    print("test123 done")
}

