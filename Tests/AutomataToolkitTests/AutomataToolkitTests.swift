import XCTest
@testable import AutomataToolkit

fileprivate var count = 0

fileprivate final class Ying: AutomataAction {
    static func requiredDataList() -> [String] {
        return []
    }

    static func returnedDataList() -> [String] {
        return []
    }

    func actionPerformed(data: AutomataDataDictionary, completionCallback: @escaping AutomataActionConclusionCallback) {
        print("Ying")
        count += 1
        completionCallback(YingYangInputs.YingStateDoneOK.rawValue, data)
    }
}

fileprivate final class Done: AutomataAction {
    static func requiredDataList() -> [String] {
        return []
    }

    static func returnedDataList() -> [String] {
        return []
    }

    func actionPerformed(data: AutomataDataDictionary, completionCallback: @escaping AutomataActionConclusionCallback) {
        count += 1
        print("Done")

        completionCallback(YingYangInputs.YingStateDoneOK.rawValue, data)
    }
}

fileprivate final class Yang: AutomataAction {
    static func requiredDataList() -> [String] {
        return []
    }

    static func returnedDataList() -> [String] {
        return []
    }

    func actionPerformed(data: AutomataDataDictionary, completionCallback: @escaping AutomataActionConclusionCallback) {
        print("Yang")
        count += 1
        completionCallback(YingYangInputs.YingStateDoneOK.rawValue, data)
    }
}

enum YingYangStates: AutomataState {
    case YingState
    case YangState
    case DoneState
}

enum YingYangInputs: AutomataInput {
    case YingStateDoneOK
    case YangStateDoneOK
    case DoneInput
}


class AutomataToolkitTests: XCTestCase {
    func testSetup() {

        let YingStateDoneOKNextStateLink = NextStateLink(currentState: YingYangStates.YingState.rawValue,
                                                         stateInput: YingYangInputs.YingStateDoneOK.rawValue)

        let YangStateDoneOKNextStateLink = NextStateLink(currentState: YingYangStates.YangState.rawValue,
                                                         stateInput: YingYangInputs.YingStateDoneOK.rawValue)

        let YingYangAutomata = Automata(
            configurationMap: [YingStateDoneOKNextStateLink: YingYangStates.YangState.rawValue,
                               YangStateDoneOKNextStateLink: YingYangStates.DoneState.rawValue],
            initialState: YingYangStates.YingState.rawValue,
            stateMap:[YingYangStates.YingState.rawValue: Ying.self,
                      YingYangStates.YangState.rawValue: Yang.self,
                      YingYangStates.DoneState.rawValue: Done.self]
        )

        let executor = AutomataExcutor(automata: YingYangAutomata)
        executor.run(data: [:])

        print("will wait for end")
        let timedout = executor.waitTillExecutionCompletes(timeout: .now() + .seconds(5))

        print("machine finished")

        XCTAssert(count == 3, "failed , not enough states visited")
        XCTAssert(timedout == true, "wait failed")
    }

    static var allTests = [
        ("testExample", testSetup),
    ]
}
