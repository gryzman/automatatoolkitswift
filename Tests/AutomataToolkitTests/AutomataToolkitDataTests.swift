//
//  AutomataToolkitDataTests.swift
//  AutomataToolkitPackageTests
//
//  Created by Gregg Jaskiewicz on 09/02/2018.
//

import XCTest
@testable import AutomataToolkit

fileprivate enum StepsDataNames: String {
    case StepOneInputData
    case StepTwoInputData
    case StepThreeInputData
    case StepOneOutputData
    case StepTwoOutputData
    case StepThreeOutputData

    case GlobalPassingData
}

fileprivate final class StepOne: AutomataAction {

    static func requiredDataList() -> [String] {
        return [
            StepsDataNames.StepOneInputData.rawValue,
            StepsDataNames.GlobalPassingData.rawValue
        ]
    }

    static func returnedDataList() -> [String] {
        return [StepsDataNames.StepOneOutputData.rawValue,
                StepsDataNames.GlobalPassingData.rawValue]
    }

    func actionPerformed(data: AutomataDataDictionary, completionCallback: @escaping AutomataActionConclusionCallback) {
        print("StepOne")

        completionCallback(DataStepsInputs.StepOneOK.rawValue, data)
    }
}

fileprivate final class StepTwo: AutomataAction {
    static func requiredDataList() -> [String] {
        return [StepsDataNames.StepTwoInputData.rawValue,
                StepsDataNames.GlobalPassingData.rawValue]
    }

    static func returnedDataList() -> [String] {
        return [StepsDataNames.StepTwoOutputData.rawValue,
                StepsDataNames.GlobalPassingData.rawValue]
    }

    func actionPerformed(data: AutomataDataDictionary, completionCallback: @escaping AutomataActionConclusionCallback) {
        print("StepTwo")
        completionCallback(DataStepsInputs.StepTwoOK.rawValue, data)
    }
}


fileprivate final class StepThree: AutomataAction {
    static func requiredDataList() -> [String] {
        return [StepsDataNames.StepThreeInputData.rawValue,
                StepsDataNames.GlobalPassingData.rawValue]
    }

    static func returnedDataList() -> [String] {
        return [StepsDataNames.StepThreeOutputData.rawValue,
                StepsDataNames.GlobalPassingData.rawValue]
    }

    func actionPerformed(data: AutomataDataDictionary, completionCallback: @escaping AutomataActionConclusionCallback) {
        print("StepThree")

        completionCallback(DataStepsInputs.StepThreeOK.rawValue, data)
    }
}

enum DataStepsStates: AutomataState {
    case StepOneState
    case StepTwoState
    case StepThreeState
}

enum DataStepsInputs: AutomataInput {
    case StepOneOK
    case StepTwoOK
    case StepThreeOK
}


class AutomataToolkitPackageTests: XCTestCase {
    func testData() {

        print("test data")

        let StepOneStateOK = NextStateLink(currentState: DataStepsStates.StepOneState.rawValue,
                                           stateInput: DataStepsInputs.StepOneOK.rawValue)

        let StepTwoStateOK = NextStateLink(currentState: DataStepsStates.StepTwoState.rawValue,
                                           stateInput: DataStepsInputs.StepTwoOK.rawValue)

        //        let StepThreeStateOK = NextStateLink(currentState: DataStepsStates.StepThreeState.rawValue,
        //                                           stateInput: DataStepsInputs.StepThreeOK.rawValue)

        let configurationMap = [
            StepOneStateOK: DataStepsStates.StepTwoState.rawValue,
            StepTwoStateOK: DataStepsStates.StepThreeState.rawValue
        ]

        let stateMap: [AutomataState : AutomataAction.Type] = [
            DataStepsStates.StepOneState.rawValue: StepOne.self,
            DataStepsStates.StepTwoState.rawValue: StepTwo.self,
            DataStepsStates.StepThreeState.rawValue: StepThree.self
        ]

        let DataTestAutomata = Automata(
            configurationMap: configurationMap,
            initialState: DataStepsStates.StepOneState.rawValue,
            stateMap: stateMap
        )

        let x = AutomataExcutor(automata: DataTestAutomata)

        let inputData: AutomataDataDictionary = [
            "GlobalPassingData": "data1",
            "StepOneInputData": "data2",
            "StepTwoInputData": "data3",
            "StepThreeInputData": "data4"
        ]

        x.run(data: inputData)

        print("will wait for end")
        let timedout = x.waitTillExecutionCompletes(timeout: .now() + .seconds(5))

        print("machine finished")

        XCTAssert(timedout == true, "wait failed")
    }

    static var allTests = [
        ("testData", testData),
        ]
}
