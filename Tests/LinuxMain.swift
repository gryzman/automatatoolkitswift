import XCTest
@testable import AutomataToolkitTests

XCTMain([
    testCase(AutomataToolkitTests.allTests),
])
