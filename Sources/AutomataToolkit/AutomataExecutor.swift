//
//  AutomataExecutor.swift
//  AutomataToolkit
//
//  Created by Gregg Jaskiewicz on 09/02/2018.
//  Copyright © 2018 Gregg Jaskiewicz. All rights reserved.
//
//  Standard MIT Licence
//

import Foundation

final public class AutomataExcutor {
    private let automata: Automata
    private var currentState: AutomataState
    private var initialState: AutomataState

    private let group = DispatchGroup()

    public init(automata: Automata) {
        self.automata = automata
        self.currentState = automata.initialState
        self.initialState = automata.initialState
    }

    public func run(data: AutomataDataDictionary) {
        self.group.enter()
        assert(self.currentState == self.initialState, "can't run me twice, you know")
        self.currentData = data
        self.enterState(state: self.initialState)
    }

    public func waitTillExecutionCompletes(timeout: DispatchTime) -> Bool {
        let outcome = self.group.wait(timeout: timeout)

        return outcome == .success
    }

    public func waitTillExecutionIsCompleted() {
        self.group.wait()
    }

    // Returns data dictionary, once execution is completed.
    // If execution is not completed, returns nil
    // Does not block
    public func completedData() -> AutomataDataDictionary? {
        if self.group.wait(wallTimeout: .now()) == .success {
            return self.currentData
        }

        return nil
    }

//    private func verifyBranch(startingState: AutomataState, input: AutomataInput) -> Bool {
//        return true
//    }
//
//    private func verifyDataDependencies() -> Bool {
//        // get all possible permutations of configurationMap
//        for configurationMapPair in self.automata.configurationMap {
//
//            guard let toStateActionObjectClass = self.automata.stateMap[configurationMapPair.value] else {
//                print("state \(configurationMapPair.value) does not exists in the state map")
//                return false
//            }
//
//            guard let fromStateActionObjectClass = self.automata.stateMap[configurationMapPair.key.currentState] else {
//                print("state \(configurationMapPair.key.currentState) does not exists in the state map")
//                return false
//            }
//
//            let fromOutputData = fromStateActionObjectClass.returnedDataList()
//
//            let toInputData = toStateActionObjectClass.requiredDataList()
//
//            // now see if expected input data is provided by the output
//            let fromOutputSet = Set<String>(fromOutputData)
//            let toInputSet = Set<String>(toInputData)
//
//            guard fromOutputSet.isSubset(of: toInputSet) == true else {
//                print("required data is missing when transitioning from state \(configurationMapPair.key.currentState) to  \(configurationMapPair.value)")
//                return false
//            }
//        }
//
////        var currentState = self.initialState
//        // get next state
//
//        return true
//    }

    // TODO: develop a map of required data list, for any given branch. So we can discard data that is no longer necessary
    // for now, we are holding on to everything - which is not exactly memory efficient

    private var currentData: AutomataDataDictionary = [:]

    private func enterState(state: AutomataState) {

        let stateActionInstanceType = self.automata.stateMap[state]
        guard let stateActionInstanceTypeClass = stateActionInstanceType else {
            self.group.leave()
            return
        }

        // check if we had all required data, otherwise bail and error
        let availableSet = Set<String>(self.currentData.keys)
        let toInputSet = Set<String>(stateActionInstanceTypeClass.requiredDataList())

        guard toInputSet.isSubset(of: availableSet) == true else {
            self.group.leave()
            print("AUTOMATA ERROR: required data is missing when transitioning to state: \(state), required: \(toInputSet), available: \(availableSet)")
            return
        }

        // Get required data
        let stateActionInstance = stateActionInstanceTypeClass.init()

        //AutomataInput, AutomataDataDictionary) ->
        stateActionInstance.actionPerformed(data: self.currentData, completionCallback: { returnInput, returnData in

            self.currentData = self.currentData.merging(returnData) { (_, new) in new }

            // find next state
            let nextStateLink = NextStateLink(currentState: state, stateInput: returnInput)
            let nextState = self.automata.configurationMap[nextStateLink]

            guard let newState = nextState else {
                assert(true, "AUTOMATA ERROR: I guess I don't know what to do next")
                self.group.leave()
                return
            }

            DispatchQueue.global().async { [weak self] in
                guard let strongSelf = self else {
                    print("AUTOMATA ERROR: automata executor is deallocated")
                    return
                }

                strongSelf.enterState(state: newState)
            }
        })
    }
}
