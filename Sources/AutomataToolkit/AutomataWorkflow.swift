//
//  AutomataWorkflow.swift
//  AutomataToolkitPackageDescription
//
//  Created by Gregg Jaskiewicz on 21/02/2018.
//

import Foundation

// Workflow allows external state machine to control your code execution
// using lambda callback

public typealias AutomataWorkflowInput = Int
public typealias AutomataWorkflowState = Int

public struct WorkflowJunction {
    let currentState: AutomataWorkflowState
    let newState: AutomataWorkflowState
    let input: AutomataWorkflowInput
}

public protocol WorkflowExecutor {
    func begin()
    func setStateChangeCallback(callOnUIThread: Bool, _ callback: @escaping (_ : AutomataWorkflowState) -> ())
    func input(_ input: AutomataWorkflowInput) -> AutomataWorkflowState
    func currentState() -> AutomataWorkflowState?
}

public func WorkflowExecutorGenerator(junctions: [WorkflowJunction], initialState: AutomataWorkflowState) -> WorkflowExecutor {
    let executor = DefaultWorkflowExecutor(junctions: junctions, initialState: initialState)

    return executor
}

private final class DefaultWorkflowExecutor: WorkflowExecutor {

    final private class NextStateLink: Hashable {
        let currentState: AutomataWorkflowState
        let stateInput: AutomataWorkflowInput

        public required init(currentState: AutomataWorkflowState, stateInput: AutomataWorkflowInput) {
            self.currentState = currentState
            self.stateInput = stateInput
        }

        public var hashValue: Int {
            let hashString = "\(self.currentState)_\(self.stateInput)"

            return hashString.hashValue
        }

        public static func ==(lhs: NextStateLink, rhs: NextStateLink) -> Bool {
            return (lhs.currentState == rhs.currentState) &&
                (lhs.stateInput == rhs.stateInput)
        }
    }

    private let nextStateLink: [NextStateLink: AutomataWorkflowState]
    private var currentWorkflowState: AutomataWorkflowState
    private var stateChangeCallback: (_ : AutomataWorkflowState) -> () = { _ in }
    private var running: Bool = false
    private var executeOnMainThread: Bool = true

    private func executeCallback() {
        let state = self.currentWorkflowState

        if self.executeOnMainThread {
            DispatchQueue.main.async {
                self.stateChangeCallback(state)
            }
        } else {
            DispatchQueue.global(qos: .default) .async {
                self.stateChangeCallback(state)
            }
        }
    }

    func begin() {
        guard self.running == false else {
            return
        }

        self.running = true
        self.executeCallback()
    }

    init(junctions: [WorkflowJunction], initialState: AutomataWorkflowState) {

        var nextStateLink: [NextStateLink: AutomataWorkflowState] = [:]
        for junction in junctions {
            let nextState = NextStateLink(currentState: junction.currentState, stateInput: junction.input)
            nextStateLink[nextState] = junction.newState
        }

        self.nextStateLink = nextStateLink
        self.currentWorkflowState = initialState
    }

    func currentState() -> AutomataWorkflowState? {
        return self.currentWorkflowState
    }

    func input(_ input: AutomataWorkflowInput) -> AutomataWorkflowState  {
        let nextLink = NextStateLink(currentState: self.currentWorkflowState, stateInput: input)

        if let newState = self.nextStateLink[nextLink] {
            self.currentWorkflowState = newState

            self.executeCallback()

            return newState
        }

        return self.currentWorkflowState
    }

    func setStateChangeCallback(callOnUIThread: Bool, _ callback: @escaping (AutomataWorkflowState) -> ()) {
        self.executeOnMainThread = callOnUIThread
        self.stateChangeCallback = callback
    }
}


