//
//  AutomataToolkit.swift
//  AutomataToolkit
//
//  Created by Gregg Jaskiewicz on 28/01/2018.
//  Copyright © 2018 Gregg Jaskiewicz. All rights reserved.
//
//  Standard MIT Licence
//

import Foundation

public typealias AutomataDataDictionary = [String: Any]

public typealias AutomataActionConclusionCallback = (AutomataInput, AutomataDataDictionary) -> ()

public protocol AutomataAction {
    init()
    func actionPerformed(data: AutomataDataDictionary, completionCallback: @escaping AutomataActionConclusionCallback )
    static func requiredDataList() -> [String]
    static func returnedDataList() -> [String]
}


public class AutomataState : Equatable, ExpressibleByIntegerLiteral, Hashable {
    let state: Int

    public static func == (lhs: AutomataState, rhs: AutomataState) -> Bool {
        return (lhs.state == rhs.state)
    }

    public var hashValue: Int {
        let hashString = "\(self.state)"

        return hashString.hashValue
    }

    public required init(integerLiteral value: Int) {
        self.state = value
    }
}

public class AutomataInput : Equatable, ExpressibleByIntegerLiteral, Hashable {
    let input: Int

    public static func == (lhs: AutomataInput, rhs: AutomataInput) -> Bool {
        return (lhs.input == rhs.input)
    }

    public var hashValue: Int {
        let hashString = "\(self.input)"

        return hashString.hashValue
    }

    public required init(integerLiteral value: Int) {
        self.input = value
    }
}

final public class NextStateLink: Hashable {
    let currentState: AutomataState
    let stateInput: AutomataInput

    public required init(currentState: AutomataState, stateInput: AutomataInput) {
        self.currentState = currentState
        self.stateInput = stateInput
    }

    public var hashValue: Int {
        let hashString = "\(self.currentState)_\(self.stateInput)"

        return hashString.hashValue
    }

    public static func ==(lhs: NextStateLink, rhs: NextStateLink) -> Bool {
        return (lhs.currentState == rhs.currentState) &&
            (lhs.stateInput == rhs.stateInput)
    }
}

// describes what is the next step
public typealias AutomataConfigurationMap = [NextStateLink: AutomataState]

public struct Automata {
    public let configurationMap: AutomataConfigurationMap
    public let initialState: AutomataState
    public let stateMap: [AutomataState: AutomataAction.Type]

    public init(configurationMap: AutomataConfigurationMap, initialState: AutomataState, stateMap: [AutomataState: AutomataAction.Type]) {
        self.configurationMap = configurationMap
        self.initialState = initialState
        self.stateMap = stateMap
    }
}
