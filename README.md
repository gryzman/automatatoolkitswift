Welcome to Automata, the simple FSM framework in swift. 

With *Automata* - you can build your workflow into a state machine, which comprises of simple, easy to understand and test states. Blocks of code which are position independent.
When entering next state - block is executed, and when ready - returning an input to the state machine. The state machine then decides - based on the state and input, what is the next state to execute.



